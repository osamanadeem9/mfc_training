
// MFCDrawApp2.h : main header file for the MFCDrawApp2 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMFCDrawApp2App:
// See MFCDrawApp2.cpp for the implementation of this class
//

class CMFCDrawApp2App : public CWinAppEx
{
public:
	CMFCDrawApp2App() noexcept;


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMFCDrawApp2App theApp;
