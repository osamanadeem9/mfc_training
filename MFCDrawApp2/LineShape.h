#pragma once
#include "Shape.h"
class LineShape : public Shape
{
public:
	LineShape(CPoint StartingPoint, CPoint EndingPoint)
		: Shape(StartingPoint, EndingPoint) {}

	virtual void DrawShape(CWnd* pWnd);

};

