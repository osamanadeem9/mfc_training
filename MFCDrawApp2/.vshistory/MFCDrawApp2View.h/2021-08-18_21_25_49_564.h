
// MFCDrawApp2View.h : interface of the CMFCDrawApp2View class
//

#pragma once


class CMFCDrawApp2View : public CView
{
protected: // create from serialization only
	CMFCDrawApp2View() noexcept;
	DECLARE_DYNCREATE(CMFCDrawApp2View)

// Attributes
public:
	CMFCDrawApp2Doc* GetDocument() const;
	void OnLButtonDown(UINT i, CPoint Point);
	void OnLButtonUp(UINT i, CPoint Cur);
	void DrawViewsFromSavedFile();
	void OnMouseMove(UINT Flag, CPoint Point);
	int Xaxis, Yaxis;
	int Style = 0;		// 0 for Rectangle, 1 for Line and 2 for Circle

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CMFCDrawApp2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnDrawCircle();
	afx_msg void OnDrawRectangle();
	afx_msg void OnDrawLine();
	void DisplayMessageBox(char* shape);
};

#ifndef _DEBUG  // debug version in MFCDrawApp2View.cpp
inline CMFCDrawApp2Doc* CMFCDrawApp2View::GetDocument() const
   { return reinterpret_cast<CMFCDrawApp2Doc*>(m_pDocument); }
#endif

