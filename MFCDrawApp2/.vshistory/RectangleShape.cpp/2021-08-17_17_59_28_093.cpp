#include "pch.h"
#include "RectangleShape.h"


void RectangleShape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(pWnd);
	obj.Rectangle(X_starting_axis, Y_starting_axis, X_ending_axis, Y_ending_axis);
	
}
