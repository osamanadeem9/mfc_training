#include "pch.h"
#include "RectangleShape.h"


void Shape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(pWnd);
	CPen Pen;

	Pen.CreatePen(0, 5, RGB(0, 0, 0));
	obj.SelectObject(&Pen);

	obj.Rectangle(X_starting_axis, Y_starting_axis, X_ending_axis, Y_ending_axis);
	
}
