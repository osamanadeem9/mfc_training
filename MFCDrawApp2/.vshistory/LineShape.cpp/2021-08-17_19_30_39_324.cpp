#include "pch.h"
#include "LineShape.h"

void LineShape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(this);
	obj.MoveTo(X_starting_axis, Y_starting_axis);
	obj.LineTo(X_ending_axis, Y_ending_axis);
}
