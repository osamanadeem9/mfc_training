
// MFCDrawApp2Doc.h : interface of the CMFCDrawApp2Doc class
//


#pragma once
#include <vector>
#include <map>
#include "Shape.h"
#include <list>

using namespace std;

class CMFCDrawApp2Doc : public CDocument
{
protected: // create from serialization only
	CMFCDrawApp2Doc() noexcept;
	DECLARE_DYNCREATE(CMFCDrawApp2Doc);

// Attributes;
public:
	int sample_int;
	//map<char*, vector<int*>> shapes_coordinates_list;
	std::vector <Shape> shapes_list;
// Operations
public:
// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~CMFCDrawApp2Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
