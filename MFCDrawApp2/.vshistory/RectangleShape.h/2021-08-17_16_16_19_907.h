#pragma once
#include "Shape.h"
class RectangleShape : public Shape
{
public:
	RectangleShape(int x_starting_axis, int y_starting_axis, int x_ending_axis, int y_ending_axis) {
		Shape(x_starting_axis,  y_starting_axis, x_ending_axis, y_ending_axis);
	}

	virtual void DrawShape(CClientDC obj);
};

