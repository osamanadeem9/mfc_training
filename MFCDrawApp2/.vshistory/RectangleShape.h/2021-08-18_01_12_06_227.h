#pragma once
#include "Shape.h"
class RectangleShape : public Shape
{
public:
	RectangleShape(CPoint StartingPoint, CPoint EndingPoint)
		: Shape(StartingPoint, EndingPoint) {}
	

	virtual void DrawShape(CWnd* pWnd);

};

