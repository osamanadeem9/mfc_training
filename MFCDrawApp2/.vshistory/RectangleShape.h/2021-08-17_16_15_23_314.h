#pragma once
#include "Shape.h"
class RectangleShape : public Shape
{
public:
	RectangleShape(int x_starting_axis, int y_starting_axis, int x_ending_axis, int y_ending_axis) {
		X_starting_axis = x_starting_axis;
		Y_starting_axis = y_starting_axis;
		X_ending_axis = x_ending_axis;
		Y_ending_axis = y_ending_axis;
	}

	virtual void DrawShape(CClientDC obj);
};

