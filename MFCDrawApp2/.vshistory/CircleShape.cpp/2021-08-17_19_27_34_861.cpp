#include "pch.h"
#include "CircleShape.h"

void CircleShape::DrawShape(CClientDC obj)
{
	obj.Ellipse(X_starting_axis, Y_starting_axis, X_ending_axis, Y_ending_axis);
}
