#include "pch.h"
#include "Shape.h"

void Shape::DisplayCoordinates() {
	CString t;

	t.Format(_T("%d %d %d %d"), StartingPoint.x, StartingPoint.y, EndingPoint.x, EndingPoint.y);
	AfxMessageBox(t);

}

void Shape::DrawShape(CWnd *pWnd) {

}
void Shape::Serialize(CArchive& ar)
{
    if (ar.IsStoring())
        ar << StartingPoint << EndingPoint;
    else // Loading, not storing
        ar >> StartingPoint >> EndingPoint;
}