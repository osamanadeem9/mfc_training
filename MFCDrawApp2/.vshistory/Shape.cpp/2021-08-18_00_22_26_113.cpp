#include "pch.h"
#include "Shape.h"
#include <iostream>

void Shape::DisplayCoordinates() {
	CString t;

	t.Format(_T("%d %d %d %d"), StartingPoint.x, StartingPoint.y, EndingPoint.x, EndingPoint.y);
	AfxMessageBox(t);

}

void Shape::DrawShape(CWnd *pWnd) {
    std::cout << "Hello";
}
void Shape::Serialize(CArchive& ar)
{
    CObject::Serialize(ar);
    if (ar.IsStoring())
        ar << StartingPoint << EndingPoint;
    else // Loading, not storing
        ar >> StartingPoint >> EndingPoint;
}