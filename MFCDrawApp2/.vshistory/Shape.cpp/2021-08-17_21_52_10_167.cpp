#include "pch.h"
#include "Shape.h"

void Shape::DisplayCoordinates() {
	CString t;

	t.Format(_T("%d %d %d %d"), X_starting_axis, Y_starting_axis, X_ending_axis, Y_ending_axis);
	AfxMessageBox(t);

}

void Shape::DrawShape(CWnd *pWnd) {

}
void Shape::Serialize(CArchive& ar)
{
    if (ar.IsStoring())
        ar << X_starting_axis << Y_starting_axis << X_ending_axis << Y_ending_axis;
    else // Loading, not storing
        ar >> X_starting_axis >> Y_starting_axis >> X_ending_axis >> Y_ending_axis;;
}