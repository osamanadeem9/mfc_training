
// MFCDrawApp2Doc.cpp : implementation of the CMFCDrawApp2Doc class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp2.h"
#endif

#include "MFCDrawApp2Doc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#include <iostream>

// CMFCDrawApp2Doc

IMPLEMENT_DYNCREATE(CMFCDrawApp2Doc, CDocument)

BEGIN_MESSAGE_MAP(CMFCDrawApp2Doc, CDocument)
END_MESSAGE_MAP()


// CMFCDrawApp2Doc construction/destruction

CMFCDrawApp2Doc::CMFCDrawApp2Doc() noexcept
{
	// TODO: add one-time construction code here

}

CMFCDrawApp2Doc::~CMFCDrawApp2Doc()
{
}

BOOL CMFCDrawApp2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMFCDrawApp2Doc serialization

void CMFCDrawApp2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		//ar << &shapes_list;
		int count = shapes_list.size();
		ar << count;
		for (int i = 0; i < count; i++)
			ar << &shapes_list.at(i);
	}
	else
	{
		// TODO: add loading code here
		//ar >> shapes_list;;
		//int count = shapes_list.size();
		//ar >> count;
		//for (int i = 0; i < count; i++)
		//	ar >> shapes_list.at(i);
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CMFCDrawApp2Doc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CMFCDrawApp2Doc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data.
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CMFCDrawApp2Doc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = nullptr;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != nullptr)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMFCDrawApp2Doc diagnostics

#ifdef _DEBUG
void CMFCDrawApp2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFCDrawApp2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMFCDrawApp2Doc commands
