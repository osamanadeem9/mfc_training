#pragma once
class Shape: public CObject
{
	Shape() = delete;

protected:
	CPoint StartingPoint;
	CPoint EndingPoint;

public:
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;
	}

	void DisplayCoordinates();

	virtual void DrawShape(CWnd* pWnd);

	void Serialize(CArchive& ar);

};

