#pragma once
class Shape: public CObject
{
DECLARE_SERIAL(Shape);

protected:
	CPoint StartingPoint;
	CPoint EndingPoint;

public:
	Shape();
	virtual ~Shape();
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;
	}

	void DisplayCoordinates();

	void DrawShape(CWnd* pWnd);

	void Serialize(CArchive& ar);

};

