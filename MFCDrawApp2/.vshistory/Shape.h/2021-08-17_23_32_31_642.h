#pragma once
class Shape: public CObject
{
protected:
	CPoint StartingPoint;
	CPoint EndingPoint;

public:
	Shape(){}
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;
	}

	void DisplayCoordinates();

	virtual void DrawShape(CWnd* pWnd);

};

