#pragma once
class Shape
{
//DECLARE_SERIAL(Shape);

public:
	CPoint StartingPoint;
	CPoint EndingPoint;
	int shapeID;	// 0 for rect, 1 for line and 2 for ellipse

public:
	Shape();
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;
	}

	void DisplayCoordinates();

	virtual void DrawShape(CWnd* pWnd);

	void Serialize(CArchive& ar);

};

