#pragma once
class Shape: CObject
{

DECLARE_SERIAL(Shape)

public:
	CPoint StartingPoint;
	CPoint EndingPoint;

	Shape(){}
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;
	}

	void DisplayCoordinates();

	virtual void DrawShape(CWnd* pWnd);

	void Serialize(CArchive& ar);

};

