
// MFCDrawApp2View.cpp : implementation of the CMFCDrawApp2View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp2.h"
#endif

#include "MFCDrawApp2Doc.h"
#include "MFCDrawApp2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#include <iostream>
#include <string>
#include "RectangleShape.h"
#include "CircleShape.h"
#include "LineShape.h"
using namespace std;


// CMFCDrawApp2View

IMPLEMENT_DYNCREATE(CMFCDrawApp2View, CView)

BEGIN_MESSAGE_MAP(CMFCDrawApp2View, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_DRAW_CIRCLE, &CMFCDrawApp2View::OnDrawCircle)
	ON_COMMAND(ID_DRAW_RECTANGLE, &CMFCDrawApp2View::OnDrawRectangle)
	ON_COMMAND(ID_DRAW_LINE, &CMFCDrawApp2View::OnDrawLine)
	ON_WM_ACTIVATE()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

// CMFCDrawApp2View construction/destruction

CMFCDrawApp2View::CMFCDrawApp2View() noexcept
{
	// TODO: add construction code here

}

CMFCDrawApp2View::~CMFCDrawApp2View()
{
}

BOOL CMFCDrawApp2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCDrawApp2View drawing

void CMFCDrawApp2View::OnDraw(CDC* pDC)
{
	CMFCDrawApp2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CString t;
	t.Format(_T("%d"), pDoc->shapes_list.size());

	pDC->TextOutW(100, 100, t);
	
	if (pDoc->load_from_serialize)
		DrawViewsFromSavedFile();
}

void CMFCDrawApp2View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCDrawApp2View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCDrawApp2View diagnostics

#ifdef _DEBUG
void CMFCDrawApp2View::AssertValid() const
{
	CView::AssertValid();
}

void CMFCDrawApp2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCDrawApp2Doc* CMFCDrawApp2View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCDrawApp2Doc)));
	return (CMFCDrawApp2Doc*)m_pDocument;
}
#endif //_DEBUG

// CMFCDrawApp2View message handlers

void CMFCDrawApp2View::OnLButtonDown(UINT uint, CPoint point) {
	Xaxis = point.x;
	Yaxis = point.y;
}

void CMFCDrawApp2View::OnLButtonUp(UINT uint, CPoint endingPoint) {
	CClientDC Obj(this);
	//CPen Pen;
	//Pen.CreatePen(0, 5, RGB(0, 0, 0));
	//Obj.SelectObject(&Pen);

	CMFCDrawApp2Doc* pDoc = GetDocument();
	CPoint startingPoint = CPoint(Xaxis, Yaxis);
	Shape* shape;
	if (Style == 0) {
		RectangleShape rect = RectangleShape(startingPoint, endingPoint);
		shape = &rect;	
	}
	else if (Style == 1) {
		LineShape line = LineShape(startingPoint, endingPoint);
		shape = &line;
	}
	else {
		CircleShape circle = CircleShape(startingPoint, endingPoint);
		shape = &circle;
	}
	shape->DrawShape(this);
	//shape->DisplayCoordinates();
	pDoc->shapes_list.push_back(*shape);

	Xaxis = endingPoint.x;
	Yaxis = endingPoint.y;
}

void CMFCDrawApp2View::DrawViewsFromSavedFile() {
	CMFCDrawApp2Doc* pDoc = GetDocument();

	if (pDoc->shapes_list.size() > 0) {
		for (auto shape : pDoc->shapes_list) {
			shape = RectangleShape(shape.StartingPoint, shape.EndingPoint);
			shape.DrawShape(this);
		}

		pDoc->load_from_serialize = false;
		//Invalidate();
		//UpdateWindow();
	}
}
void CMFCDrawApp2View::OnMouseMove(UINT Flag, CPoint point) {
}



void CMFCDrawApp2View::OnDrawCircle()
{
	Style = 2;
}


void CMFCDrawApp2View::OnDrawRectangle()
{
	Style = 0;
}


void CMFCDrawApp2View::OnDrawLine()
{
	Style = 1;
	//string message = "Line is selected";
	//string title = "Choose Shape";
	//MessageBoxA(NULL, message.c_str(), title.c_str(), MB_OK);
}
