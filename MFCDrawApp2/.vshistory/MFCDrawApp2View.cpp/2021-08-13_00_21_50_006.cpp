
// MFCDrawApp2View.cpp : implementation of the CMFCDrawApp2View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp2.h"
#endif

#include "MFCDrawApp2Doc.h"
#include "MFCDrawApp2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCDrawApp2View

IMPLEMENT_DYNCREATE(CMFCDrawApp2View, CView)

BEGIN_MESSAGE_MAP(CMFCDrawApp2View, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CMFCDrawApp2View construction/destruction

CMFCDrawApp2View::CMFCDrawApp2View() noexcept
{
	// TODO: add construction code here

}

CMFCDrawApp2View::~CMFCDrawApp2View()
{
}

BOOL CMFCDrawApp2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCDrawApp2View drawing

void CMFCDrawApp2View::OnDraw(CDC* /*pDC*/)
{
	CMFCDrawApp2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}

void CMFCDrawApp2View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCDrawApp2View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCDrawApp2View diagnostics

#ifdef _DEBUG
void CMFCDrawApp2View::AssertValid() const
{
	CView::AssertValid();
}

void CMFCDrawApp2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCDrawApp2Doc* CMFCDrawApp2View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCDrawApp2Doc)));
	return (CMFCDrawApp2Doc*)m_pDocument;
}
#endif //_DEBUG


// CMFCDrawApp2View message handlers
