#include "pch.h"
#include "RectangleShape.h"


// defines DrawShape method for rectangle, with CWnd pointer as the parameter
void RectangleShape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(pWnd);
	obj.Rectangle(StartingPoint.x, StartingPoint.y, EndingPoint.x, EndingPoint.y);
	
}
