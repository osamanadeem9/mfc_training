#pragma once
#include "Shape.h"
class CircleShape : public Shape
{
public:
	CircleShape(CPoint StartingPoint, CPoint EndingPoint)
		: Shape(StartingPoint, EndingPoint) {}
	virtual void DrawShape(CWnd* pWnd);

};

