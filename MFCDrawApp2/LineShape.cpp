#include "pch.h"
#include "LineShape.h"


// defines DrawShape method for line, with CWnd pointer as the parameter
void LineShape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(pWnd);
	obj.MoveTo(StartingPoint);
	obj.LineTo(EndingPoint);
}
