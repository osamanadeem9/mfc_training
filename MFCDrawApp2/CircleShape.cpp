#include "pch.h"
#include "CircleShape.h"


// defines DrawShape method for circle, with CWnd pointer as the parameter
void CircleShape::DrawShape(CWnd* pWnd)
{
	CClientDC obj(pWnd);
	obj.Ellipse(StartingPoint.x, StartingPoint.y, EndingPoint.x, EndingPoint.y);
}
