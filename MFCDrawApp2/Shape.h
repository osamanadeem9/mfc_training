#pragma once
class Shape
{
//DECLARE_SERIAL(Shape);

protected:
	CPoint StartingPoint;
	CPoint EndingPoint;
	int shapeID;	// 0 for rect, 1 for line and 2 for ellipse

public:
	Shape();
	Shape(CPoint startingPoint, CPoint endingPoint) {
		StartingPoint = startingPoint;
		EndingPoint = endingPoint;	
		shapeID = 0;	// initialized shapeID to our default shape selection i.e. rectangle
	}

	CPoint getStartingPoint() { return StartingPoint; }
	CPoint getEndingPoint() { return EndingPoint; }
	int getShapeID() { return shapeID; }
	void setShapeID(int shapeId) { shapeID = shapeId; }

	void DisplayCoordinates();	// method for displaying coordinates of the shape drawn

	virtual void DrawShape(CWnd* pWnd);		// method for drawing any selected shape

	//void Serialize(CArchive& ar);

};

