
// MFCDrawAppView.cpp : implementation of the CMFCDrawAppView class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp.h"
#endif

#include "MFCDrawAppDoc.h"
#include "MFCDrawAppView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCDrawAppView

IMPLEMENT_DYNCREATE(CMFCDrawAppView, CView)

BEGIN_MESSAGE_MAP(CMFCDrawAppView, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CMFCDrawAppView construction/destruction

CMFCDrawAppView::CMFCDrawAppView() noexcept
{
	// TODO: add construction code here

}

CMFCDrawAppView::~CMFCDrawAppView()
{
}

BOOL CMFCDrawAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCDrawAppView drawing

void CMFCDrawAppView::OnDraw(CDC* pDC)
{
	CMFCDrawAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	// TODO: add draw code for native data here

	const std::vector<CPoint> linePoints = pDoc->GetLinePoints();

	int sizeLinePoints = linePoints.size();
	if (sizeLinePoints > 0) {
		CPen pen(PS_SOLID, 5, RGB(0, 255, 0));
		CPen* oldPen = pDC->SelectObject(&pen);
	
		pDC->MoveTo(linePoints[0]);
		for (int i = 1; i < sizeLinePoints; ++i) {
			pDC->LineTo(linePoints[i]);

		}
		pDC->SelectObject(oldPen);
	}

	pDC->Rectangle(10, 10, 20, 20);
}

void CMFCDrawAppView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCDrawAppView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCDrawAppView diagnostics

#ifdef _DEBUG
void CMFCDrawAppView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCDrawAppView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCDrawAppDoc* CMFCDrawAppView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCDrawAppDoc)));
	return (CMFCDrawAppDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCDrawAppView message handlers

void CMFCDrawAppView::OnLButtonDown(UINT nFlags, CPoint point) {

	LONG x = point.x;
	LONG y = point.y;
	//CMFCDrawAppDoc* pDoc = GetDocument();
	//ASSERT_VALID(pDoc);
	//if (!pDoc)
	//	return;

	//pDoc->AddLinePont(point);
	//Invalidate();
	//UpdateWindow();

	//CView::OnLButtonDown(nFlags, point);
}
