
// MFCDrawAppView.cpp : implementation of the CMFCDrawAppView class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp.h"
#endif

#include "MFCDrawAppDoc.h"
#include "MFCDrawAppView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCDrawAppView

IMPLEMENT_DYNCREATE(CMFCDrawAppView, CView)

BEGIN_MESSAGE_MAP(CMFCDrawAppView, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CMFCDrawAppView construction/destruction

CMFCDrawAppView::CMFCDrawAppView() noexcept
{
	// TODO: add construction code here

}

CMFCDrawAppView::~CMFCDrawAppView()
{
}

BOOL CMFCDrawAppView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCDrawAppView drawing

void CMFCDrawAppView::OnDraw(CDC* pDC)
{
	CMFCDrawAppDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	// TODO: add draw code for native data here
	pDC->LineTo(200, 88);
}

void CMFCDrawAppView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCDrawAppView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCDrawAppView diagnostics

#ifdef _DEBUG
void CMFCDrawAppView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCDrawAppView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCDrawAppDoc* CMFCDrawAppView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCDrawAppDoc)));
	return (CMFCDrawAppDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCDrawAppView message handlers
