
// MFCDrawAppView.h : interface of the CMFCDrawAppView class
//

#pragma once


class CMFCDrawAppView : public CView
{
protected: // create from serialization only
	CMFCDrawAppView() noexcept;
	DECLARE_DYNCREATE(CMFCDrawAppView)

// Attributes
public:
	CMFCDrawAppDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CMFCDrawAppView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFCDrawAppView.cpp
inline CMFCDrawAppDoc* CMFCDrawAppView::GetDocument() const
   { return reinterpret_cast<CMFCDrawAppDoc*>(m_pDocument); }
#endif

