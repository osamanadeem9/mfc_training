
// MFCDrawAppDoc.cpp : implementation of the CMFCDrawAppDoc class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCDrawApp.h"
#endif

#include "MFCDrawAppDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMFCDrawAppDoc

IMPLEMENT_DYNCREATE(CMFCDrawAppDoc, CDocument)

BEGIN_MESSAGE_MAP(CMFCDrawAppDoc, CDocument)
END_MESSAGE_MAP()


// CMFCDrawAppDoc construction/destruction

CMFCDrawAppDoc::CMFCDrawAppDoc() noexcept
{
	// TODO: add one-time construction code here

}

CMFCDrawAppDoc::~CMFCDrawAppDoc()
{
}

BOOL CMFCDrawAppDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CMFCDrawAppDoc serialization

void CMFCDrawAppDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		/*int size = m_linePoints.size();
		ar << size;
		for (auto point : m_linePoints) {
			ar << point.x << point.y;
		}*/
	}
	else
	{
		// TODO: add loading code here
		/*int size;
		ar >> size;
		CPoint point;
		for (int i = 0; i < size; ++i) {
			ar >> point.x >> point.y;
			m_linePoints.push_back(point);
		}*/
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void CMFCDrawAppDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void CMFCDrawAppDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data.
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void CMFCDrawAppDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = nullptr;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != nullptr)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMFCDrawAppDoc diagnostics

#ifdef _DEBUG
void CMFCDrawAppDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMFCDrawAppDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMFCDrawAppDoc commands
