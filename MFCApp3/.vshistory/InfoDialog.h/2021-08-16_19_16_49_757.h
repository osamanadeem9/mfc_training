#pragma once


// InfoDialog dialog

class InfoDialog : public CDialog
{
	DECLARE_DYNAMIC(InfoDialog)

public:
	InfoDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~InfoDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_INFO };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_name;
	CString m_email;
	int m_age;
	CString m_city;
	CString m_zip;
};
