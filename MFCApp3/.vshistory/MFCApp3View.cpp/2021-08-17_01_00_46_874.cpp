
// MFCApp3View.cpp : implementation of the CMFCApp3View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCApp3.h"
#endif

#include "MFCApp3Doc.h"
#include "MFCApp3View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#include "InfoDialog.h"

// CMFCApp3View

IMPLEMENT_DYNCREATE(CMFCApp3View, CView)

BEGIN_MESSAGE_MAP(CMFCApp3View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMFCApp3View::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_PRESS_ME_CLICK, onPressMeClick)
	ON_COMMAND(ID_OPTIONS_EDITINFO, &CMFCApp3View::OnOptionsEditinfo)
END_MESSAGE_MAP()

// CMFCApp3View construction/destruction

CMFCApp3View::CMFCApp3View() noexcept
{
	// TODO: add construction code here

}

CMFCApp3View::~CMFCApp3View()
{
}

BOOL CMFCApp3View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCApp3View drawing

void CMFCApp3View::OnDraw(CDC* pDC)
{
	CMFCApp3Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CString result;
	pDC->TextOutW(50, 50, result.Format("Cats: %d, Dogs = %d", num_cats, num_dogs));
	//CRect deviceRect;
	//GetClientRect(deviceRect);

	//if (!mRedColor) {
	//	pDC->FillSolidRect(deviceRect, RGB(255, 0, 0));
	//}
	//else {
	//	pDC->FillSolidRect(deviceRect, RGB(255, 255, 255));
	//}
	// TODO: add draw code for native data here
}


// CMFCApp3View printing


void CMFCApp3View::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CMFCApp3View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMFCApp3View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMFCApp3View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CMFCApp3View::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCApp3View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCApp3View diagnostics

#ifdef _DEBUG
void CMFCApp3View::AssertValid() const
{
	CView::AssertValid();
}

void CMFCApp3View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCApp3Doc* CMFCApp3View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCApp3Doc)));
	return (CMFCApp3Doc*)m_pDocument;
}
#endif //_DEBUG


// CMFCApp3View message handlers


int CMFCApp3View::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	/*m_button.Create(_T("To Red"), BS_LEFTTEXT, CRect(100, 100, 200, 200), this, IDC_BUTTON_PRESS_ME_CLICK);
	m_button.ShowWindow(SW_SHOW);*/

	return 0;
}

void CMFCApp3View::onPressMeClick() {
	//AfxMessageBox(_T("Heelo"));

	if (!mRedColor) {
		m_button.SetWindowTextW(_T("To Red"));
	}
	else {
		m_button.SetWindowTextW(_T("To White"));
	}
	mRedColor = !mRedColor;
	Invalidate();
	UpdateWindow();
}

void CMFCApp3View::OnOptionsEditinfo()
{
	InfoDialog editInfoDialog;
	int res = editInfoDialog.DoModal();

	if (res == IDOK) {
		AfxMessageBox(editInfoDialog.m_final_info);
	}
}
