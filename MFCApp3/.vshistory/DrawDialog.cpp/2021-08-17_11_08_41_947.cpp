// DrawDialog.cpp : implementation file
//

#include "pch.h"
#include "MFCApp3.h"
#include "DrawDialog.h"
#include "afxdialogex.h"


// DrawDialog dialog

IMPLEMENT_DYNAMIC(DrawDialog, CDialog)

DrawDialog::DrawDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DIALOG_DRAW, pParent)
{

}

DrawDialog::~DrawDialog()
{
}

void DrawDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DrawDialog, CDialog)
END_MESSAGE_MAP()


// DrawDialog message handlers
