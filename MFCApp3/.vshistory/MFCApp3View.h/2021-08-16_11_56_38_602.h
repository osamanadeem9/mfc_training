
// MFCApp3View.h : interface of the CMFCApp3View class
//

#pragma once


class CMFCApp3View : public CView
{
protected: // create from serialization only
	CMFCApp3View() noexcept;
	DECLARE_DYNCREATE(CMFCApp3View)

// Attributes
public:
	CMFCApp3Doc* GetDocument() const;
	CButton m_button;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CMFCApp3View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};

#ifndef _DEBUG  // debug version in MFCApp3View.cpp
inline CMFCApp3Doc* CMFCApp3View::GetDocument() const
   { return reinterpret_cast<CMFCApp3Doc*>(m_pDocument); }
#endif

