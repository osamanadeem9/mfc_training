#pragma once


// DrawDialog dialog

class DrawDialog : public CDialog
{
	DECLARE_DYNAMIC(DrawDialog)

public:
	DrawDialog(CWnd* pParent = nullptr);   // standard constructor
	virtual ~DrawDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_DRAW };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
