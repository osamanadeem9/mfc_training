// InfoDialog.cpp : implementation file
//

#include "pch.h"
#include "MFCApp3.h"
#include "InfoDialog.h"
#include "afxdialogex.h"


// InfoDialog dialog

IMPLEMENT_DYNAMIC(InfoDialog, CDialog)

InfoDialog::InfoDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DIALOG_INFO, pParent)
{

}

InfoDialog::~InfoDialog()
{
}

void InfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(InfoDialog, CDialog)
END_MESSAGE_MAP()


// InfoDialog message handlers
