// InfoDialog.cpp : implementation file
//

#include "pch.h"
#include "MFCApp3.h"
#include "InfoDialog.h"
#include "afxdialogex.h"


// InfoDialog dialog

IMPLEMENT_DYNAMIC(InfoDialog, CDialog)

InfoDialog::InfoDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DIALOG_INFO, pParent)
	, m_name(_T(""))
	, m_email(_T(""))
	, m_age(0)
	, m_city(_T(""))
	, m_zip(_T(""))
{

}

InfoDialog::~InfoDialog()
{
}

void InfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	DDX_Text(pDX, IDC_EDIT_EMAIL, m_email);
	DDX_Text(pDX, IDC_EDIT_AGE, m_age);
	DDV_MinMaxInt(pDX, m_age, 0, 120);
	DDX_Text(pDX, IDC_EDIT_CITY, m_city);
	DDX_Text(pDX, IDC_EDIT_ZIP, m_zip);
}


BEGIN_MESSAGE_MAP(InfoDialog, CDialog)
END_MESSAGE_MAP()


// InfoDialog message handlers
