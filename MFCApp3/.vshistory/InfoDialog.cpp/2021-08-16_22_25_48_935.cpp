// InfoDialog.cpp : implementation file
//

#include "pch.h"
#include "MFCApp3.h"
#include "InfoDialog.h"
#include "afxdialogex.h"


// InfoDialog dialog

IMPLEMENT_DYNAMIC(InfoDialog, CDialog)

InfoDialog::InfoDialog(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DIALOG_INFO, pParent)
	, m_name(_T(""))
	, m_email(_T(""))
	, m_age(0)
	, m_city(_T(""))
	, m_zip(_T(""))
	, m_final_info(_T(""))
{

}

InfoDialog::~InfoDialog()
{
}

void InfoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_NAME, m_name);
	DDX_Text(pDX, IDC_EDIT_EMAIL, m_email);
	DDX_Text(pDX, IDC_EDIT_AGE, m_age);
	DDV_MinMaxInt(pDX, m_age, 0, 120);
	DDX_Text(pDX, IDC_EDIT_CITY, m_city);
	DDX_Text(pDX, IDC_EDIT_ZIP, m_zip);
	DDX_Text(pDX, IDC_EDIT_FINAL_INFO, m_final_info);
}


BEGIN_MESSAGE_MAP(InfoDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_VERIFY, &InfoDialog::OnBnClickedButtonVerify)
END_MESSAGE_MAP()


// InfoDialog message handlers


void InfoDialog::OnBnClickedButtonVerify()
{
	m_final_info.Format(_T("%s \n %s"), m_name, m_email);
		
		//m_name + "\n" +
		//m_email + "\n" +
		//m_age + "\n";

	Invalidate();
	UpdateData();
	UpdateWindow();
	
}
