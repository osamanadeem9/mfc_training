
// MFCApp2View.cpp : implementation of the CMFCApp2View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCApp2.h"
#endif

#include "MFCApp2Doc.h"
#include "MFCApp2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCApp2View

IMPLEMENT_DYNCREATE(CMFCApp2View, CView)

BEGIN_MESSAGE_MAP(CMFCApp2View, CView)
END_MESSAGE_MAP()

// CMFCApp2View construction/destruction

CMFCApp2View::CMFCApp2View() noexcept
{
	// TODO: add construction code here

}

CMFCApp2View::~CMFCApp2View()
{
}

BOOL CMFCApp2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCApp2View drawing

void CMFCApp2View::OnDraw(CDC* pDC)
{
	CMFCApp2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	CRect rect;
	GetClientRect(&rect);
	x_dcMem.FillSolidRect(rect, RGB(255, 255, 255));

	int count = pDoc->m_arrShape.GetSize();
	for (int i = 0; i < count; i++) {
		pDoc->m_arrShape[i]->onDraw(&x_dcMem);
	}

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &x_dcMem, 0, 0, SRCCOPY);
	//pDC->Rectangle(100, 100, 200, 200);
	// TODO: add draw code for native data here
}


// CMFCApp2View diagnostics

#ifdef _DEBUG
void CMFCApp2View::AssertValid() const
{
	CView::AssertValid();
}

void CMFCApp2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCApp2Doc* CMFCApp2View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCApp2Doc)));
	return (CMFCApp2Doc*)m_pDocument;
}

#endif //_DEBUG

// CMFCApp2View message handlers

int CMFCApp2View::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CClientDC dc(this); // add 'this' in the constructor
	int x = GetSystemMetrics(SM_CXSCREEN);
	int y = GetSystemMetrics(SM_CYSCREEN);

	x_dcMem.CreateCompatibleDC(&dc);
	x_bitmapMem.CreateCompatibleBitmap(&dc, x, y);

	x_dcMem.SelectObject(&x_bitmapMem);

	return 0;
}

void CMFCApp2View::OnLButtonDown(UINT nFlags, CPoint point) {
	
	CMFCApp2Doc* doc = GetDocument();
	ShapeClass* shape = new ShapeClass(point, point);
	doc->m_arrShape.Add(shape);

	CView::OnLButtonDown(nFlags, point);


}

void CMFCApp2View::OnMouseMove(UINT nFlags, CPoint point) {
	if (nFlags == MK_LBUTTON) {
		long mouseX = point.x;
		long mouseY = point.y;
		//CMFCApp2Doc* doc = GetDocument();
		//int count = doc->m_arrShape.GetSize();
		//doc->m_arrShape[count - 1]->bottomright = point;
		//doc->SetModifiedFlag(TRUE);
		//Invalidate();
		CView::OnMouseMove(nFlags, point);
	}
}
