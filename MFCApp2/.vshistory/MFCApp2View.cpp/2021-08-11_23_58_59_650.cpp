
// MFCApp2View.cpp : implementation of the CMFCApp2View class
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "MFCApp2.h"
#endif

#include "MFCApp2Doc.h"
#include "MFCApp2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCApp2View

IMPLEMENT_DYNCREATE(CMFCApp2View, CView)

BEGIN_MESSAGE_MAP(CMFCApp2View, CView)
END_MESSAGE_MAP()

// CMFCApp2View construction/destruction

CMFCApp2View::CMFCApp2View() noexcept
{
	// TODO: add construction code here

}

CMFCApp2View::~CMFCApp2View()
{
}

BOOL CMFCApp2View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CMFCApp2View drawing

void CMFCApp2View::OnDraw(CDC* pDC)
{
	CMFCApp2Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	pDC->Rectangle(10, 10, 20, 20);
	// TODO: add draw code for native data here
}


// CMFCApp2View diagnostics

#ifdef _DEBUG
void CMFCApp2View::AssertValid() const
{
	CView::AssertValid();
}

void CMFCApp2View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCApp2Doc* CMFCApp2View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCApp2Doc)));
	return (CMFCApp2Doc*)m_pDocument;
}

#endif //_DEBUG


// CMFCApp2View message handlers
