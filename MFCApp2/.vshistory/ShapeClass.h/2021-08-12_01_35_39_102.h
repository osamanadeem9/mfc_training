#pragma once
#include <afx.h>
class ShapeClass : public CObject
{
public:
	ShapeClass(CPoint topleft, CPoint bottomright);
	virtual ~ShapeClass();

	CPoint topleft, bottomright;
	void onDraw(CDC* pdc);
};

