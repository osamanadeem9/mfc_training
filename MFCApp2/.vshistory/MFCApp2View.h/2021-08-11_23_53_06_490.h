
// MFCApp2View.h : interface of the CMFCApp2View class
//

#pragma once


class CMFCApp2View : public CView
{
protected: // create from serialization only
	CMFCApp2View() noexcept;
	DECLARE_DYNCREATE(CMFCApp2View)

// Attributes
public:
	CMFCApp2Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CMFCApp2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MFCApp2View.cpp
inline CMFCApp2Doc* CMFCApp2View::GetDocument() const
   { return reinterpret_cast<CMFCApp2Doc*>(m_pDocument); }
#endif

