#include "pch.h"
#include "ShapeClass.h"

ShapeClass::ShapeClass(CPoint topleft, CPoint bottoomright) {
	this->topleft = topleft;
	this->bottomright = bottomright;
}

ShapeClass:: ~ShapeClass() {

}

void ShapeClass::onDraw(CDC* dc) {
	CPen pen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* oldPen = dc->SelectObject(&pen);

	CRect rect(topleft, bottomright);
	CPoint arr[] = {
		CPoint(rect.left, rect.CenterPoint().y),
		CPoint(rect.CenterPoint().x, rect.top),
		CPoint(rect.right, rect.CenterPoint().y),
		CPoint(rect.CenterPoint().x, rect.bottom)

	};
	dc->Polygon(arr, 4);

	dc->SelectObject(oldPen);
	oldPen->DeleteObject();
}
