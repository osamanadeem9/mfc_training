
// MFCApp2.h : main header file for the MFCApp2 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CMFCApp2:
// See MFCApp2.cpp for the implementation of this class
//

class CMFCApp2 : public CWinApp
{
public:
	CMFCApp2() noexcept;


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CMFCApp2 theApp;
